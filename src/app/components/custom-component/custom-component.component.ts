import { Component, OnInit } from '@angular/core';

type direction = 'center' | 'left' | 'top' | 'right' | 'bottom';

@Component({
  selector: 'app-custom-component',
  templateUrl: './custom-component.component.html',
  styleUrls: ['./custom-component.component.scss']
})
export class CustomComponentComponent implements OnInit {
  position: direction; // Направление растягивания / движения
  innerYOnMouseDown: number; // Внутренний отступ при клике по Y
  innerXOnMouseDown: number; // Внутренний отступ при клике по X
  outerXOnMouseMove: number; // Внешний отступ от левой границы при растягивании
  outerYOnMouseMove: number; // Внешний отступ от верхней границы при растягивании
  differenceX: number; // Дельта X
  defaultWidth: number; // Начальная ширина
  defaultLeftPosition: number; // Начальная позиция по left
  differenceY: number; // Дельта Y
  defaultHeight: number; // Начальная высота
  defaultTopPosition: number; // Начальная позиция по top
/**
 * Меняет вид курсора при наведении на разные части квадрата
 * @param event MouseMove
 */
  setDirection(event: any): void {
    const targetSquare = event.target;
    const coords: { x: number, y: number } = {
      x: event.clientX - event.offsetX,
      y: event.clientY - event.offsetY
    };

    if (event.offsetX < 10) {
      targetSquare.style.cursor = 'ew-resize';
      this.position = 'left';
    } else if (targetSquare.offsetWidth - event.offsetX < 10) {
      targetSquare.style.cursor = 'ew-resize';
      this.position = 'right';
    } else if (event.offsetY < 10) {
      targetSquare.style.cursor = 'ns-resize';
      this.position = 'top';
    } else if (targetSquare.offsetHeight - event.offsetY < 10) {
      targetSquare.style.cursor = 'ns-resize';
      this.position = 'bottom';
    } else {
      targetSquare.style.cursor = 'move';
      this.position = 'center';
    }
  }
/**
 * Выбирает дальнейший метод для трансформации (движение / растягивание) в зависимости от положения курсора на квадрате
 * @param position Направление растягивания / движения
 * @param event MouseDown
 */
  transform(position: direction, event: any): void {
    switch (position) {
      case 'center':
        this.move(event);
        break;

      case 'bottom':
        this.resizeBottom(event);
        break;

      case 'top':
        this.resizeTop(event);
        break;

      case 'left':
        this.resizeLeft(event);
        break;

      case 'right':
        this.resizeRight(event);
        break;
    }
  }
/**
 * Двигает квадрат за мышкой
 * @param someEvent MouseDown
 */
  move(someEvent: any): void {
    /**
     * Определить внутренние отступы
     */
    this.innerYOnMouseDown = someEvent.offsetY;
    this.innerXOnMouseDown = someEvent.offsetX;
    /**
     * Задать события на элемент document, иначе событие прерывается
     */
    document.onmousemove = (e): void => {
      document.onmouseup = (): void => {
        this.resetEvent(someEvent.target);
      };
      someEvent.target.style.top = e.pageY - this.innerYOnMouseDown + 'px';
      someEvent.target.style.left = e.pageX - this.innerXOnMouseDown + 'px';
    };
  }
  /**
   * Меняет размер при клике на нижней границе
   * @param someEvent MouseDown
   */
  resizeBottom(someEvent: any): void {
    this.outerYOnMouseMove = someEvent.pageY - someEvent.target.offsetHeight;
    this.defaultHeight = someEvent.target.offsetHeight;
    this.defaultTopPosition = parseInt(window.getComputedStyle(someEvent.target).getPropertyValue('top'), 10);
    document.onmousemove = (e): void => {
      document.onmouseup = (): void => {
        this.resetEvent(someEvent.target);
      };
      this.differenceY = e.pageY - this.outerYOnMouseMove - this.defaultHeight;
      someEvent.target.style.height = this.defaultHeight + (this.differenceY * 2) + 'px';
      someEvent.target.style.top = this.defaultTopPosition - this.differenceY + 'px';
    };
  }
  /**
   * Меняет размер при клике на верхней границе
   * @param someEvent MouseDown
   */
  resizeTop(someEvent: any): void {
    this.outerYOnMouseMove = someEvent.pageY - someEvent.target.offsetHeight;
    this.defaultHeight = someEvent.target.offsetHeight;
    this.defaultTopPosition = parseInt(window.getComputedStyle(someEvent.target).getPropertyValue('top'), 10);
    document.onmousemove = (e): void => {
      document.onmouseup = (): void => {
        this.resetEvent(someEvent.target);
      };
      this.differenceY = e.pageY - this.outerYOnMouseMove - this.defaultHeight;
      someEvent.target.style.height = this.defaultHeight - (this.differenceY * 2) + 'px';
      someEvent.target.style.top = this.defaultTopPosition + this.differenceY + 'px';
    };
  }
  /**
   * Меняет размер при клике на левой границе
   * @param someEvent MouseDown
   */
  resizeLeft(someEvent: any): void {
    this.outerXOnMouseMove = someEvent.pageX - someEvent.target.offsetWidth;
    this.defaultWidth = someEvent.target.offsetWidth;
    this.defaultLeftPosition = parseInt(window.getComputedStyle(someEvent.target).getPropertyValue('left'), 10);
    document.onmousemove = (e): void => {
      document.onmouseup = (): void => {
        this.resetEvent(someEvent.target);
      };
      this.differenceX = e.pageX - this.outerXOnMouseMove - this.defaultWidth;
      someEvent.target.style.width = this.defaultWidth - (this.differenceX * 2) + 'px';
      someEvent.target.style.left = this.defaultLeftPosition + this.differenceX + 'px';
    };
  }
  /**
   * Меняет размер при клике на правой границе
   * @param someEvent MouseDown
   */
  resizeRight(someEvent: any): void {
    this.outerXOnMouseMove = someEvent.pageX - someEvent.target.offsetWidth;
    this.defaultWidth = someEvent.target.offsetWidth;
    this.defaultLeftPosition = parseInt(window.getComputedStyle(someEvent.target).getPropertyValue('left'), 10);
    document.onmousemove = (e): void => {
      document.onmouseup = (): void => {
        this.resetEvent(someEvent.target);
      };
      this.differenceX = e.pageX - this.outerXOnMouseMove - this.defaultWidth;
      someEvent.target.style.width = this.defaultWidth + (this.differenceX * 2) + 'px';
      someEvent.target.style.left = this.defaultLeftPosition - this.differenceX + 'px';
    };
  }

  /**
   * Обнуляет события
   * @param elem целевой элемент
   */
  resetEvent(elem: HTMLElement): void {
    document.onmousemove = null;
    elem.onmouseup = null;
  }

  constructor() { }

  ngOnInit() {
  }

}
