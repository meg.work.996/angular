import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SquareTaskComponent } from './square-task.component';

describe('SquareTaskComponent', () => {
  let component: SquareTaskComponent;
  let fixture: ComponentFixture<SquareTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SquareTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SquareTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
