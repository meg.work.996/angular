import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-square-task',
  templateUrl: './square-task.component.html',
  styleUrls: ['./square-task.component.scss']
})
export class SquareTaskComponent implements OnInit {

  public generatedStyles;
  public getStyles(style) {
    this.generatedStyles = style;
  }

  constructor() { }

  ngOnInit() {
  }

}
