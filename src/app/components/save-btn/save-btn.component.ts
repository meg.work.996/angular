import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-save-btn',
  templateUrl: './save-btn.component.html',
  styleUrls: ['./save-btn.component.scss']
})
export class SaveBtnComponent implements OnInit {

  public commonStyles: string[];
  public generatedStyles: CSSStyleDeclaration;
  @Output() eventSender: EventEmitter<object> = new EventEmitter();

  public generateStyles() {
    this.commonStyles = [];
    this.generatedStyles = window.getComputedStyle(document.querySelector('.square'));
    this.commonStyles.push('width: ' + this.generatedStyles.getPropertyValue('width'));
    this.commonStyles.push('height: ' + this.generatedStyles.getPropertyValue('height'));
    this.commonStyles.push('background: ' + this.generatedStyles.getPropertyValue('background'));
    this.commonStyles.push('border-radius: ' + this.generatedStyles.getPropertyValue('border-radius'));
    this.commonStyles.push('z-index: ' + this.generatedStyles.getPropertyValue('z-index'));
    this.commonStyles.push('transition: ' + this.generatedStyles.getPropertyValue('box-shadow'));
    this.commonStyles.push('position: ' + this.generatedStyles.getPropertyValue('position'));
    this.commonStyles.push('left: ' + this.generatedStyles.getPropertyValue('left'));
    this.commonStyles.push('top: ' + this.generatedStyles.getPropertyValue('top'));
    this.eventSender.emit(this.commonStyles);
  }

  constructor() { }

  ngOnInit() {
  }

}
