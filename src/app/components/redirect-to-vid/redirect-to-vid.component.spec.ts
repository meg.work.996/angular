import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectToVidComponent } from './redirect-to-vid.component';

describe('RedirectToVidComponent', () => {
  let component: RedirectToVidComponent;
  let fixture: ComponentFixture<RedirectToVidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirectToVidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectToVidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
