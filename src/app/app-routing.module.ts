import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VideoCompComponent } from './modules/video-page/components/video-comp/video-comp.component';
import { SquareTaskComponent } from './components/square-task/square-task.component';

const routes: Routes = [
    {
        path: 'video',
        component: VideoCompComponent
    },
    {
        path: '',
        component: SquareTaskComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
