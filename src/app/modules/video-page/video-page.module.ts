import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoCompComponent } from './components/video-comp/video-comp.component';
import { RedirectToMainComponent } from './components/redirect-to-main/redirect-to-main.component';
import { AppRoutingModule } from '../../app-routing.module';



@NgModule({
  declarations: [VideoCompComponent, RedirectToMainComponent],
  imports: [
    CommonModule,
    AppRoutingModule
  ]
})
export class VideoPageModule { }
