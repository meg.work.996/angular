import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoCompComponent } from './video-comp.component';

describe('VideoCompComponent', () => {
  let component: VideoCompComponent;
  let fixture: ComponentFixture<VideoCompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoCompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
