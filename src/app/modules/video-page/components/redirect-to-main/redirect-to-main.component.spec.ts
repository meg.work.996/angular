import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectToMainComponent } from './redirect-to-main.component';

describe('RedirectToMainComponent', () => {
  let component: RedirectToMainComponent;
  let fixture: ComponentFixture<RedirectToMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirectToMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectToMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
