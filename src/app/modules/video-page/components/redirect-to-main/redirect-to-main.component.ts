import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-redirect-to-main',
  templateUrl: './redirect-to-main.component.html',
  styleUrls: ['./redirect-to-main.component.scss']
})
export class RedirectToMainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
