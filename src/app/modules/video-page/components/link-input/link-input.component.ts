import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-link-input',
  templateUrl: './link-input.component.html',
  styleUrls: ['./link-input.component.scss']
})
export class LinkInputComponent implements OnInit {

  changeBtnText(): void {
    document.querySelector('.linkSendButton').nodeValue = 'Скрыть';
  }

  showPreview(): void {
    console.log('Showing preview!');
  }

  constructor() { }

  ngOnInit() {
  }

}
