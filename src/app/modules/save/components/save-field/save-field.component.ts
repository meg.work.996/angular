import { Component, OnInit, Output, Input } from '@angular/core';

@Component({
  selector: 'app-save-field',
  templateUrl: './save-field.component.html',
  styleUrls: ['./save-field.component.scss']
})
export class SaveFieldComponent implements OnInit {

  @Input() public computedStyles;

  constructor() { }

  ngOnInit() {
  }

}
