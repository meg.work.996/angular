import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveFieldComponent } from './components/save-field/save-field.component';
import { BrowserModule } from '@angular/platform-browser';


@NgModule({
  imports: [CommonModule, BrowserModule],
  declarations: [SaveFieldComponent],
  exports: [SaveFieldComponent]
})
export class SaveModule { }
