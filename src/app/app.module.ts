import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CustomComponentComponent } from './components/custom-component/custom-component.component';
import { SaveBtnComponent } from './components/save-btn/save-btn.component';
import { SaveModule } from './modules/save/save.module';
import { RedirectToVidComponent } from './components/redirect-to-vid/redirect-to-vid.component';
import { AppRoutingModule } from './app-routing.module';
import { VideoPageModule } from './modules/video-page/video-page.module';
import { SquareTaskComponent } from './components/square-task/square-task.component';


@NgModule({
  declarations: [
    AppComponent,
    CustomComponentComponent,
    SaveBtnComponent,
    RedirectToVidComponent,
    SquareTaskComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    SaveModule,
    AppRoutingModule,
    VideoPageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
